/*
 * Copyright 2020 <copyright holder> <email>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include <Eigen/Dense>
#include <qp_solver/qp_solver.hpp>

#include "pinocchio/algorithm/frames.hpp"
#include "pinocchio/algorithm/jacobian.hpp"
#include "pinocchio/algorithm/joint-configuration.hpp"
#include "pinocchio/algorithm/kinematics.hpp"
#include "pinocchio/algorithm/model.hpp"
#include "pinocchio/fwd.hpp"
#include "pinocchio/multibody/model.hpp"
#include "pinocchio/parsers/urdf.hpp"

namespace QPController {
class Velocity {

public:
  enum RegularisationType { MinimizeVelocity, TrackConfiguration };

  /**
   * \fn bool init
   * \brief Initializes the controller
   * \param std::string robot_description a URDF description of the robot
   * \param std::vector<std::string> joint_names a list of joint_name to be
   * controlled \return true if the controller is correctly initialized
   */
  bool init(std::string robot_description,
            std::vector<std::string> joint_names = std::vector<std::string>());

  /**
   * \fn Eigen::VectorXd update
   * \brief Update the controller to get the new desired joint torque to send to
   * the robot. \param Eigen::VectorXd q the current joint position of the robot
   * \param Eigen::VectorXd qd the current joint velocity of the robot
   * \param Eigen::Matrix<double,6,1> xd_star the desired Cartesian velocity
   * \return A Eigen::VectorXd with the desired joint velocity
   */
  Eigen::VectorXd update(Eigen::VectorXd q, Eigen::VectorXd qd,
                         Eigen::Matrix<double, 6, 1> xd_star);

  /**
   * \fn void setControlledFrame
   * \brief Set the control frame to be considered in pinocchio
   * \param std::string controlled_frame the controlled_frame name
   */
  void setControlledFrame(std::string controlled_frame);

  /**
   * \fn void setRegularizationGains
   * \brief Set the regularization gains
   * \param Eigen::VectorXd regularisation_gains the regularization gains
   */
  void setRegularizationGains(Eigen::VectorXd p_gains_qd);

  /**
   * \fn void setRegularizationWeight
   * \brief Set the regularization task weight
   * \param double regularisation_weight the regularization weight
   */
  void setRegularizationWeight(double regularisation_weight);

  /**
   * \fn void setRegularizationConfiguration
   * \brief Set the reference configuration for the regularization task
   * \param Eigen::VectorXd q_ref reference configuration for the regularization
   * task
   */
  void setRegularizationConfiguration(Eigen::VectorXd q_ref);

  /**
   * \fn void setControlPeriod
   * \brief Set the control period
   * \param double control_period the control period
   */
  void setControlPeriod(double control_period);

  /**
   * \fn void setAccelerationMax
   * \brief Set the maximum acceleration
   * \param Eigen::VectorXd acceleration_max the maximum acceleration
   */
  void setAccelerationMax(Eigen::VectorXd acceleration_max);

  /**
   * \fn pinocchio::Model getRobotModel
   * \brief get a pinocchio robot model
   * \return a pinocchio robot model
   */
  pinocchio::Model getRobotModel();

  /**
   * \fn std::string getControlledFrame()
   * \brief get the control frame
   * \return the control frame
   */
  std::string getControlledFrame();

  /**
   * \fn void addRegularisationType()
   * \brief add a regularisation to the qp problem
   * \param Eigen::VectorXd q the robot configuration
   */
  void addRegularisationTask(Eigen::VectorXd q);

  /**
   * \fn void setRegularisationType(RegularisationType regularisation_type)
   * \brief set the regularisation type among MinimizeVelocity or
   * \param RegularisationType regularisation type the
   * desired type of regularisation
   */
  void setRegularisationType(RegularisationType regularisation_type);

private:
  /**
   * \fn bool is_in_vector
   * \brief check if a std::string is in a std::vector<std::string>
   * \return true if the std::string is in the std::vector<std::string>
   */
  template <typename T>
  bool is_in_vector(const std::vector<T> &vector, const T &elt) {
    return vector.end() != std::find(vector.begin(), vector.end(), elt);
  }

  /**
   * \fn bool load_robot
   * \brief Loads Panda robot
   * \param std::string robot_description a URDF description of the robot
   * \param std::vector<std::string> joint_names a list of joint_name to be
   * controlled \return true if the robot is correctly loaded
   */
  bool load_robot_model(std::string robot_description,
                        std::vector<std::string> joint_names);

  // Publishers
  pinocchio::Model model; /*!< @brief pinocchio model */
  pinocchio::Data data;   /*!< @brief pinocchio data */

  pinocchio::Data::Matrix6x J; /*!< @brief The robot Jacobian */

  double regularisation_weight;      /*!< @brief Regularisation weight */
  Eigen::VectorXd kp_regularization; /*!< @brief regularization gains */
  std::string controlled_frame;      /*!< @brief frame to be controlled*/
  bool control_frame_set =
      false; /*!< @brief check if control frame is set before init */
  double control_period; /*!< @brief Control period */
  Eigen::VectorXd q_ref; /*!< @brief Reference joint position of the robot (for
                            the regularization task*/
  Eigen::VectorXd acceleration_max; /*!< @brief Maximum allowed acceleration*/
  bool acceleration_max_is_set =
      false; /*!< @brief check if max acceleration is set before init */
  QPSolver qp_solver;     /*!< @brief a qp_solved object */
  QPSolver::qpProblem qp; /*!< @brief a qp object */

  int number_of_constraints; /*!< @brief Number of constraints of the QP
                                problem*/
  int number_of_variables;   /*!< @brief Number of optimization variables of the
                                QP problem*/
  RegularisationType regularisation_type = RegularisationType::MinimizeVelocity;
};
} // namespace QPController
