#include "velocity_qp/controller.h"

namespace QPController {
bool Velocity::init(std::string robot_description,
                    std::vector<std::string> joint_names) {
  //--------------------------------------
  // LOAD ROBOT
  //--------------------------------------
  if (!load_robot_model(robot_description, joint_names))
    return false;

  if (!control_frame_set)
    controlled_frame = model.frames[model.nframes - 1].name;
  kp_regularization = Eigen::MatrixXd::Ones(model.nv, 1) * 100.0;
  setRegularizationWeight(1e-5);
  q_ref = (model.upperPositionLimit + model.lowerPositionLimit) / 2.0;
  setControlPeriod(0.001);
  //--------------------------------------
  // INITIALIZE VARIABLES
  //--------------------------------------
  J.resize(6, model.nv);
  J.setZero();

  qp = qp_solver.configureQP(number_of_variables, number_of_constraints);
  return true;
}

void Velocity::setControlledFrame(std::string controlled_frame) {
  this->controlled_frame = controlled_frame;
  control_frame_set = true;
}

void Velocity::setAccelerationMax(Eigen::VectorXd acceleration_max) {
  this->acceleration_max = acceleration_max;
  acceleration_max_is_set = true;
}

std::string Velocity::getControlledFrame() { return controlled_frame; }

void Velocity::setRegularizationGains(Eigen::VectorXd kp_regularization) {
  if (regularisation_type == RegularisationType::TrackConfiguration) {
    this->kp_regularization = kp_regularization;
  } else {
    std::cout << "cannnot set a regularisation configuration as regularisation "
                 "type is MinimizeVelocity"
              << std::endl;
  }
}

void Velocity::setRegularizationWeight(double regularisation_weight) {
  this->regularisation_weight = regularisation_weight;
}

void Velocity::setRegularizationConfiguration(Eigen::VectorXd q_ref) {
  if (regularisation_type == RegularisationType::TrackConfiguration) {
    this->q_ref = q_ref;
  } else {
    std::cout << "cannnot set a regularisation configuration as regularisation "
                 "type is MinimizeVelocity"
              << std::endl;
  }
}

void Velocity::setControlPeriod(double control_period) {
  this->control_period = control_period;
}

Eigen::VectorXd Velocity::update(Eigen::VectorXd q, Eigen::VectorXd qd,
                                 Eigen::Matrix<double, 6, 1> xd_star) {
  // First calls the forwardKinematics on the model, then computes the placement
  // of each frame.
  pinocchio::forwardKinematics(model, data, q, qd);
  pinocchio::updateFramePlacements(model, data);
  // Compute jacobian
  pinocchio::computeJointJacobians(model, data, q);
  pinocchio::computeFrameJacobian(model, data, q,
                                  model.getFrameId(controlled_frame),
                                  pinocchio::ReferenceFrame::LOCAL, J);
  pinocchio::getFrameJacobian(model, data, model.getFrameId(controlled_frame),
                              pinocchio::ReferenceFrame::LOCAL, J);
  // Formulate QP problem such that
  // joint_velocity_out = argmin 1/2 qd^T H_ qd + qd^T g_
  //                         s.t     lbA_ < A_ qd << ubA_
  //                                     lb_ < qd < ub_

  // Main task
  qp.hessian = 2.0 * J.transpose() * J;
  qp.gradient = -2.0 * J.transpose() * xd_star;

  // Regularisation task
  addRegularisationTask(q);

  // Bounds
  if (acceleration_max_is_set) {
    qp.ub =
        model.velocityLimit.cwiseMin(acceleration_max * control_period + qd);
    qp.lb =
        -model.velocityLimit.cwiseMax(-acceleration_max * control_period + qd);
  } else {
    qp.ub = model.velocityLimit;
    qp.lb = -model.velocityLimit;
  }

  // Constraints
  double horizon_dt = 15 * control_period;
  qp.a_constraints.block(0, 0, model.nv, model.nv) =
      horizon_dt * Eigen::MatrixXd::Identity(model.nv, model.nv);
  qp.ub_a.segment(0, model.nv) = model.upperPositionLimit - q;
  qp.lb_a.segment(0, model.nv) = model.lowerPositionLimit - q;

  return qp_solver.SolveQP(qp);
}

void Velocity::setRegularisationType(RegularisationType regularisation_type) {
  this->regularisation_type = regularisation_type;
}

void Velocity::addRegularisationTask(Eigen::VectorXd q) {
  if (regularisation_type == RegularisationType::MinimizeVelocity) {
    qp.hessian += 2.0 * regularisation_weight *
                  Eigen::MatrixXd::Identity(model.nv, model.nv);
  } else if (regularisation_type == RegularisationType::TrackConfiguration) {
    qp.hessian += 2.0 * regularisation_weight *
                  Eigen::MatrixXd::Identity(model.nv, model.nv);
    qp.gradient += -2.0 * regularisation_weight *
                   kp_regularization.cwiseProduct((q_ref - q));
  }
}

pinocchio::Model Velocity::getRobotModel() { return model; }

bool Velocity::load_robot_model(std::string robot_description,
                                std::vector<std::string> joint_names) {
  // Load the urdf model
  pinocchio::Model temp_model;
  pinocchio::urdf::buildModelFromXML(robot_description, temp_model, false);
  if (!joint_names.empty()) {
    std::vector<pinocchio::JointIndex> list_of_joints_to_keep_unlocked_by_id;
    for (std::vector<std::string>::const_iterator it = joint_names.begin();
         it != joint_names.end(); ++it) {
      const std::string &joint_name = *it;
      if (temp_model.existJointName(joint_name))
        list_of_joints_to_keep_unlocked_by_id.push_back(
            temp_model.getJointId(joint_name));
      else
        std::cout << "joint: " << joint_name
                  << " does not belong to the temp_model.";
    }

    // Transform the list into a list of joints to lock
    std::vector<pinocchio::JointIndex> list_of_joints_to_lock_by_id;
    for (pinocchio::JointIndex joint_id = 1;
         joint_id < temp_model.joints.size(); ++joint_id) {
      const std::string joint_name = temp_model.names[joint_id];
      if (is_in_vector(joint_names, joint_name))
        continue;
      else {
        list_of_joints_to_lock_by_id.push_back(joint_id);
      }
    }

    // Build the reduced temp_model from the list of lock joints
    Eigen::VectorXd q_rand = randomConfiguration(temp_model);
    model = pinocchio::buildReducedModel(temp_model,
                                         list_of_joints_to_lock_by_id, q_rand);
  }
  data = pinocchio::Data(model);

  // Initialize the various solvers
  number_of_variables = model.nv;
  number_of_constraints = model.nv;

  return true;
}
} // namespace QPController
